if status is-interactive
    # Commands to run in interactive sessions can go here
end

# remove fish greeting
set -g fish_greeting

# load any-nix-shell if installed
if type -q any-nix-shell
    any-nix-shell fish | source
end

# add homebrew path for apple silicon mac
fish_add_path /opt/homebrew/bin

# add nix paths
fish_add_path /run/current-system/sw/bin
fish_add_path /etc/profiles/per-user/$USER/bin
