function fish_prompt --description 'Write out the prompt'
    set -l last_pipestatus $pipestatus
    set -lx __fish_last_status $status # Export for __fish_print_pipestatus.

    set -l color_cwd
    set -l suffix
    set -l shlvl $SHLVL

    if functions -q fish_is_root_user; and fish_is_root_user
        if set -q fish_color_cwd_root
            set color_cwd $fish_color_cwd_root
        else
            set color_cwd $fish_color_cwd
        end
        set suffix '#'
    else
        set color_cwd $fish_color_cwd
        set suffix ''
    end

    # show shell level
    if test $shlvl -gt 1
        set_color $fish_color_redirection
        printf "[%s] " $shlvl
        set_color normal
    end

    # PWD
    set_color $color_cwd
    echo -n (prompt_pwd)

    # show previous exit code if it failed
    set -l status_color (set_color $fish_color_status)
    set -l statusb_color (set_color --bold $fish_color_status)
    set -l prompt_status (__fish_print_pipestatus " [" "]" "|" "$status_color" "$statusb_color" $last_pipestatus)
    echo -n $prompt_status
    set_color normal
    echo -n "$suffix "
end
