[Appearance]
ColorScheme=Moonlight

[Cursor Options]
CursorShape=1

[General]
Command=fish
Name=Moonlight
Parent=FALLBACK/
TerminalColumns=103
TerminalRows=97

[Scrolling]
ScrollBarPosition=2

[Terminal Features]
BellMode=1
