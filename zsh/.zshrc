# .zshrc - Read when interactive

# workaround for tramp hanging when connecting to zsh
if [ $TERM = "dumb" ]
then
    unsetopt zle
    PS1='$ '
    return
fi

if [ $INSIDE_EMACS ]
then
    # Share current directory with Emacs
    chpwd() { print -P "\033AnSiTc %d" }
    print -P "\033AnSiTu %n"
    print -P "\033AnSiTc %d"
    if [ $(uname -s) '==' 'Darwin' ]
    then
        # Fix backspace inside Emacs on macOS
        stty ek
    fi
else
    # Display current directory in the terminal emulator title bar
    chpwd() {print -Pn "\e]2; %~/ \a"}
fi

# Add color to ls, based on OS
if [ $(uname -s) '=~' '[Ll]inux' ]
then
	# Linux
        alias ls='ls --color'
else
	# BSD/OS X
        alias ls='ls -G'
fi

# Set alias for easy detailed listing
alias ll='ls -l'

# Set keybindings
bindkey '\eq' push-line-or-edit
bindkey '^[[3~' delete-char-or-list #fn+delete
bindkey " " magic-space # do history expansion on space
bindkey '\eH' run-help

# Disable flow control so \C-s can search forward
setopt noflowcontrol

# Run Editor on command line with unwieldy fellows
autoload -U edit-command-line
zle -N edit-command-line
bindkey '\ee' edit-command-line

# Set cd to always pushd
setopt autopushd

# Limit dir stack size (created by pushd)
DIRSTACKSIZE=20

# Ignore duplicates in directory stack
setopt pushd_ignoredups

# Set globbing options
setopt cshnullglob
setopt no_case_glob
setopt numericglobsort
setopt extendedglob

# Set shell not to "lie" about linked directories
setopt chaselinks

# Set zsh to automatically cd when directory names are typed
setopt autocd

# Better run-help
autoload -U run-help

# Autocomplete
autoload -U compinit promptinit
compinit
promptinit

# Set word forward/backward behavior to be consistent w/ emacs
autoload -U select-word-style
select-word-style bash

#load zmv - great for file renaming
autoload -U zmv

zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
zstyle ':completion:*:warnings' format 'No matches: %d'
zstyle ':completion:*:messages' format %d
zstyle ':completion:*' list-separator '#'
zstyle ':completion:*' auto-description 'Specify: %d'

# Correction Suggestions
setopt correct

# don't add lines to history if they begin with a space
setopt HIST_IGNORE_SPACE

# Conda env name in rprompt
function pyenv_info {
    if [ $CONDA_DEFAULT_ENV ]
    then
        echo "%F{green}" $CONDA_DEFAULT_ENV "%f"
    fi
}

# RVM env details in rprompt
function rvm_info {
    if [ $GEM_HOME ]
    then
        [[ $GEM_HOME =~ ruby[-\/][0-9]\.[0-9](\.[0-9])?(-.*)?(@.*)? ]]
        echo "%F{red}" ${MATCH##ruby?} "%f"
    fi
}

# Prompt
autoload -U colors
colors

PROMPT="%F{cyan}%~%(?..%F{red} [%?])%f "
RPROMPT='$(rvm_info) $(pyenv_info) ${vcs_info_msg_0_}'"%F{magenta}%m%f"

# VCS Info
setopt prompt_subst
autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:git*' check-for-changes true
zstyle ':vcs_info:git*' formats "%F{yellow}% %r/%b %F{red}%B%u%c%%b "
zstyle ':vcs_info:git*' actionformats "%F{yellow} %r/%b %F{red}%B%u%c %F{magenta}(%a)%%b "
precmd() { vcs_info }

# History
HISTSIZE=1000
SAVEHIST=500
HISTFILE=~/.zsh_history
setopt extended_history
setopt append_history
setopt hist_ignore_dups
setopt hist_expire_dups_first
setopt hist_find_no_dups
setopt hist_reduce_blanks

# Set EmacsClient Alias and variables
function set_emacsclient_cli {
    local emacs_client="$(which emacsclient) -c -nw"
    alias ec="$emacs_client"
    export EDITOR="$emacs_client"
    export VISUAL="$emacs_client"
}
set_emacsclient_cli
export ALTERNATE_EDITOR=""

# Set Syntax highlighting for less (requires highlight)
export LESSOPEN="| $(which highlight) %s -lq --force -O xterm256 --style=base16/seti"
export LESS="-FgiJRuX"
alias more="less -Em"

# Load configs from .zshrc.d
if  [ -d $HOME/.zshrc.d/ ]
then
	for zshrc in $HOME/.zshrc.d/*.zsh; do
		test -r "$zshrc" && . "$zshrc"
	done
	unset zshrc
fi
