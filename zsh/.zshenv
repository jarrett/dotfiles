# .zshenv - Read every time

# homebrew stuff
if [[ -d "/opt/homebrew" ]]; then
    export HOMEBREW_PREFIX="/opt/homebrew"
    export HOMEBREW_CELLAR="/opt/homebrew/Cellar"
    export HOMEBREW_REPOSITORY="/opt/homebrew"
    export PATH="/opt/homebrew/bin:/opt/homebrew/sbin${PATH+:$PATH}"
    export MANPATH="/opt/homebrew/share/man${MANPATH+:$MANPATH}:"
    export INFOPATH="/opt/homebrew/share/info:${INFOPATH:-}"
fi

# set GOPATH
if [[ -d "$HOME/go" ]]; then
   export GOPATH=$HOME/go
   export PATH="$GOPATH/bin${PATH+:$PATH}"
fi

# set GEMHOME
if [[ -d "$HOME/.gem" ]]; then
    export GEM_HOME=$HOME/.gem
    export PATH="$GEM_HOME/bin${PATH+:$PATH}"
fi

# set CARGOHOME
if [[ -d "$HOME/.cargo" ]]; then
    export CARGO_HOME=$HOME/.cargo
    export PATH="$CARGO_HOME/bin${PATH+:$PATH}"
fi

# add $HOME/bin to PATH
export PATH="$HOME/bin${PATH+:$PATH}"
