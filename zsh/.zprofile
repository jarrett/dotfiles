# .zprofile - Read at login

# nothing understands tmux-256color
if [[ $TERM = "tmux-256color" ]]
then
    export TERM="xterm-256color"
fi
