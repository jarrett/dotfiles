** Just some dotfiles
*** How to use:
1. Clone this repo into your home folder.
2. Enter this directory: =$ cd ~/dotfiles=
3. Call GNU Stow to symlink the desired dotfiles: =$ stow zsh tmux=

*** How to remove:
1. Enter the dotfiles directory: =$ cd ~/dotfiles=
2. Call GNU Stow to remove the symlinks it created. =$ stow -D zsh tmux=

*** Special instructions:
=reuse_ssh_connections= requires:
- =Include ~/.ssh/config.d/*= to be added to the =~/.ssh/config= file.
- =~/.ssh/sockets= dir is created.
